import React, { useEffect, useState, useCallback, memo, useMemo } from "react"
import "./App.css"

const areEqual = (prevProps, nextProps) => prevProps !== nextProps

const Button = memo((props) => {
  console.count("render button")
  return <button {...props} style={{ backgroundColor: "lightgray" }} />
}, areEqual)

const ListItem = memo(({ children }) => {
  console.count("render list item")
  return (
    <li>
      {children}
      <label style={{ fontSize: "smaller" }}>
        <input type="checkbox" />
        Add to cart
      </label>
    </li>
  )
}, areEqual)

const List = memo(({ products }) => (
  <ul>
    {products.map(({ name, id }) => (
      <ListItem key={id}>{name}</ListItem>
    ))}
  </ul>
))

const App = () => {
  const [searchString, setSearchString] = useState("")
  const [isSortingDesc, setSortingDesc] = useState(false)
  const [products, setProducts] = useState([])

  useEffect(() => {
    ;(async () => {
      console.count("render fetch")

      const response = await fetch("https://reqres.in/api/products")
      const { data } = await response.json()

      const products = data.filter((item) => item.name.includes(searchString))

      setProducts(products)
    })()
  }, [searchString])

  const toggleSortingDesc = useCallback(
    () => setSortingDesc((value) => !value),
    []
  )
  const onSearch = useCallback((e) => setSearchString(e.target.value), [])

  const sortedProducts = useMemo(
    () =>
      products.sort((a, z) =>
        isSortingDesc
          ? z.name.localeCompare(a.name)
          : a.name.localeCompare(z.name)
      ),
    [products, isSortingDesc]
  )

  console.count("render app")

  return (
    <div className="App">
      <input type="search" value={searchString} onChange={onSearch} />
      <Button onClick={toggleSortingDesc}>Change sort direction</Button>
      <List products={sortedProducts} isSortingDesc={isSortingDesc} />
    </div>
  )
}

export default App
